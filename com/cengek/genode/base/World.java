package com.cengek.genode.base;


import com.cengek.genode.ui.Manager;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public abstract class World {
    
    protected Vector _displayList;
    protected Manager _uiManager;
    protected Rectangle _view;
    
    public World()
    {
        _displayList = new Vector();
        _view = new Rectangle(G.screen.getDimension());
        _uiManager = new Manager(this);
    }
    
    public Rectangle getView() {
        return _view;
    }
    
    protected void add(Entity entity) {
        _displayList.addElement(entity);
    }
    
    protected void remove(Entity entity) {
        _displayList.removeElement(entity);
    }
    
    public void update(long delta) {
        _uiManager.update(delta);
    }
            
    public void render(Graphics graphics) {
        int size = _displayList.size();
        for(int i = 0; i < size; i++) {
            Entity e = (Entity)_displayList.elementAt(i);
            Point ep = e.getPosition(), vp = _view.getPosition();
            Dimension es = e.getSize(), vs = _view.getSize();
            
            if(ep.x - es.width > vp.x && ep.x < vs.width &&
               ep.y - es.height > vp.y && ep.y < vs.height) {
                e.render(graphics);
            }
        }
        
        _uiManager.render(graphics);
    }
    
}
