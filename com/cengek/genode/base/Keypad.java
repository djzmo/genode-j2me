/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cengek.genode.base;

import java.util.Vector;
import javax.microedition.lcdui.game.GameCanvas;

/**
 *
 * @author Graha
 */
public class Keypad {
    
    public static final int Up = GameCanvas.UP_PRESSED;
    public static final int Right = GameCanvas.RIGHT_PRESSED;
    public static final int Down = GameCanvas.DOWN_PRESSED;
    public static final int Left = GameCanvas.LEFT_PRESSED;
    public static final int Fire = GameCanvas.FIRE_PRESSED;
    
    public static boolean isKeyPressed(int key) {
        boolean ret = false;
        
        if(key == Keypad.Up && (_keyState & key) != 0 && !_up) {
            _up = true;
            ret = true;
        }
        else if(key == Keypad.Right && (_keyState & key) != 0 && !_right) {
            _right = true;
            ret = true;
        }
        else if(key == Keypad.Down && (_keyState & key) != 0 && !_down) {
            _down = true;
            ret = true;
        }
        else if(key == Keypad.Left && (_keyState & key) != 0 && !_left) {
            _left = true;
            ret = true;
        }
        else if(key == Keypad.Fire && (_keyState & key) != 0 && !_fire) {
            _fire = true;
            ret = true;
        }
        
        return ret;
    }
    
    public static boolean isKeyDown(int key) {
        if(_keyState == key)
            return true;
        else return false;
    }
    
    public static void update() {
        _keyState = G.engine.getKeyStates();
        
        if(_up && _keyState != Keypad.Up) {
            _up = false;
        }
        else if(_right && _keyState != Keypad.Right) {
            _right = false;
        }
        else if(_down && _keyState != Keypad.Down) {
            _down = false;
        }
        else if(_left && _keyState != Keypad.Left) {
            _left = false;
        }
        else if(_fire && _keyState != Keypad.Fire) {
            _fire = false;
        }
    }
    
    private static int _keyState;
    private static boolean _up = false;
    private static boolean _right = false;
    private static boolean _down = false;
    private static boolean _left = false;
    private static boolean _fire = false;
    
}
