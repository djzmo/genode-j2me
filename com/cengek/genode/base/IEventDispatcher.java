/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cengek.genode.base;

/**
 *
 * @author Graha
 */
public class IEventDispatcher {
    
    protected IEventListener _eventListener;
    
    public void addEventListener(IEventListener eventListener) {
        _eventListener = eventListener;
    }
    
    public void removeEventListener() {
        _eventListener = null;
    }
    
}
