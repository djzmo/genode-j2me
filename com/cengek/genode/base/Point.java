package com.cengek.genode.base;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LoginError
 */
public class Point {

    public int x, y;

    public Point() {
        x = 0;
        y = 0;
    }

    public Point(Point point) {
        this.x = point.x;
        this.y = point.y;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void add(Point point) {
        this.x += point.x;
        this.y += point.y;
    }
    
    public void subtract(Point point) {
        this.x -= point.x;
        this.y -= point.y;
    }
    
    public void multiply(Point point) {
        this.x *= point.x;
        this.y *= point.y;
    }
    
    public void divide(Point point) {
        this.x /= point.x;
        this.y /= point.y;
    }
    
    public void add(int x, int y) {
        this.x += x;
        this.y += y;
    }
    
    public void subtract(int x, int y) {
        this.x -= x;
        this.y -= y;
    }
    
    public void multiply(int x, int y) {
        this.x *= x;
        this.y *= y;
    }
    
    public void divide(int x, int y) {
        this.x /= x;
        this.y /= y;
    }
    
    public void add(int n) {
        this.x += n;
        this.y += n;
    }
    
    public void subtract(int n) {
        this.x -= n;
        this.y -= n;
    }
    
    public void multiply(int n) {
        this.x *= n;
        this.y *= n;
    }
    
    public void divide(int n) {
        this.x /= n;
        this.y /= n;
    }

    public double distanceTo(Point point) {
        return distanceTo(point.x, point.y);
    }

    public double distanceTo(int x, int y) {
        int a = this.x - x;
        int b = this.y - y;

        return Math.sqrt(a * a + b * b);
    }

    public static double manhattanDistance(int x1, int y1, int x2, int y2) {
        return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }

    public static double distanceBetween(int x1, int y1, int x2, int y2) {
        int a = x1 - x2;
        int b = y1 - y2;

        return Math.sqrt(a * a + b * b);
    }
    
    public String toString() {
        return "{" + x + "," + y + "}";
    }
    
    public Point clone() {
        return new Point(x, y);
    }
    
}
