/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cengek.genode.base;

/**
 *
 * @author Graha
 */
public class Pair {
    
    public Object first;
    public Object second;
    
    public Pair() {
    }
    
    public Pair(Object obj1, Object obj2) {
        first = obj1;
        second = obj2;
    }
    
}
