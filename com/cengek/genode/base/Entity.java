package com.cengek.genode.base;


import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.Sprite;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public class Entity extends Rectangle implements IDrawable {
    
    protected long _color;
    
    public Entity() {
        super(0, 0, 0, 0);
    }
    
    public Entity(Point position, Dimension size) {
        super(position, size);
    }
    
    public Entity(int x, int y, int width, int height) {
        super(x, y, width, height);
    }
    
    public Entity(Rectangle rect) {
        super(rect);
    }
    
    public long getColor() {
        return _color;
    }
    
    public void setColor(long color) {
        _color = color;
    }
    
    protected void render(Graphics graphics, Sprite sprite) {
        sprite.setPosition(_position.x - G.world.getView().getPosition().x,
                _position.y - G.world.getView().getPosition().y);
        sprite.paint(graphics);
    }
    
    public void init() {
    }
    
    public void render(Graphics graphics) {
    }
    
    public void update(long delta) {
    }
    
}
