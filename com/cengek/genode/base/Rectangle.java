package com.cengek.genode.base;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
public class Rectangle {
    
    protected Point _position;
    protected Dimension _size;
    
    public Rectangle() {
        _position = new Point(0, 0);
        _size = new Dimension(0, 0);
    }
    
    public Rectangle(Point position, Dimension size) {
        _position = position.clone();
        _size = size.clone();
    }

    public Rectangle(int x, int y, int width, int height) {
        _position = new Point(x, y);
        _size = new Dimension(width, height);
    }
    
    public Rectangle(Point position) {
        _position = position.clone();
        _size = new Dimension(0, 0);
    }
    
    public Rectangle(Dimension size) {
        _position = new Point(0, 0);
        _size = size.clone();
    }
    
    public Rectangle(Rectangle rect) {
        _position = rect.getPosition().clone();
        _size = rect.getSize().clone();
    }
    
    public void setPosition(Point position) {
        _position = position.clone();
    }
    
    public void setPosition(int x, int y) {
        _position = new Point(x, y);
    }
    
    public void setSize(Dimension size) {
        _size = size.clone();
    }
    
    public void setSize(int width, int height) {
        _size = new Dimension(width, height);
    }
    
    public Point getPosition() {
        return _position;
    }
    
    public Dimension getSize() {
        return _size;
    }

    public boolean intersects(Rectangle rect) {   
        Point p1 = _position, p2 = rect.getPosition();
        Dimension s1 = _size, s2 = rect.getSize();
        
        int ty = (Math.abs(p1.y - p2.y + s2.height) > Math.abs(p1.y + s1.height - p2.y))
                ? Math.abs(p1.y - p2.y + s2.height) : Math.abs(p1.y + s1.height - p2.y);
        int tx = (Math.abs(p1.x - p2.x + s2.width) > Math.abs(p1.x + s1.width - p2.x))
                ? Math.abs(p1.y - p2.x + s2.width) : Math.abs(p1.x + s1.width - p2.x);
        
        return tx < (p1.x + s1.width - p1.y) + (p2.x + s2.width - p2.x) && 
                ty < (p1.y + s1.height - p1.y) + (p2.y + s2.height - p2.y);        
    }

    public boolean intersects(int x, int y) {
        return (x <= _position.x + _size.width) && (x >= _position.x) && (y >= _position.y) && (y <= _position.y + _size.height);
    }
    
    public void resize(Point position, Dimension size) {
        _position = position.clone();
        _size = size.clone();
    }
    
    public void resize(int x, int y, int width, int height) {
        _position = new Point(x, y);
        _size = new Dimension(width, height);
    }
    
    public void resize(Rectangle rect) {
        _position = rect.getPosition().clone();
        _size = rect.getSize().clone();
    }
    
    public Rectangle clone() {
        return new Rectangle(_position, _size);
    }
    
}