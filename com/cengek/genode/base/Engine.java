package com.cengek.genode.base;


import javax.microedition.lcdui.*;
import javax.microedition.lcdui.game.GameCanvas;
import javax.microedition.midlet.MIDlet;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public abstract class Engine extends GameCanvas implements Runnable {
    
    private Thread _thread;
    private MIDlet _parent;
    private Graphics _graphics;
    private boolean _running;
    
    protected Engine(MIDlet parent) {
        super(true);
        _parent = parent;
        
        setFullScreenMode(true);
        
        _graphics = getGraphics();
        _thread = new Thread(this);
        _parent = parent;
        _running = true;
    }
    
    public void init() {
        G.screen = new Screen(this);
        G.window = _graphics;
        G.engine = this;
    }
    
    public void run() {
        init();
        long startTime = System.currentTimeMillis();
        int fps = 0, fpsCounter = 0;
        while(_running) {
            long now = System.currentTimeMillis();
            long delta = now - startTime;
            startTime = now;
            
            fpsCounter += delta;
            if(fpsCounter >= 1000) {
                fpsCounter -= 1000;
                System.out.println(Integer.toString(fps));
                fps = 0;
            }
            
            Keypad.update();
            G.screen.update();
            G.world.update(delta);
            
            _graphics.setColor(0, 0, 0);
            _graphics.fillRect(0, 0, G.screen.getDimension().width, G.screen.getDimension().height);
            
            G.world.render(_graphics);
            
            flushGraphics();
            fps++;
        }
        
        _parent.notifyDestroyed();
    }
    
    public void start() {
        _thread.start();
    }
    
}
