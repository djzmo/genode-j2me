/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cengek.genode.base;

import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Graha
 */
public interface IDrawable {
    
    public abstract void render(Graphics graphics);
    public abstract void update(long delta);
    
}
