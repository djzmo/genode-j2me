package com.cengek.genode.base;


import javax.microedition.lcdui.game.GameCanvas;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public class Screen {
    
    private GameCanvas _parent;
    private Dimension _dimension;
    
    public Screen(GameCanvas parent) {
        _dimension = new Dimension();
        _parent = parent;
    }
    
    public void update() {
        _dimension.width = _parent.getWidth();
        _dimension.height = _parent.getHeight();
    }
    
    public Dimension getDimension() {
        return _dimension;
    }
    
}
