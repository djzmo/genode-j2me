/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cengek.genode.base;

/**
 *
 * @author Graha
 */
public class Color {
    
    public int r, g, b, a;
    
    public Color() {
        
    }
    
    public Color(int red, int green, int blue) {
        r = red;
        g = green;
        b = blue;
        a = 255;
    }
    
    public Color(int red, int green, int blue, int alpha) {
        r = red;
        g = green;
        b = blue;
        a = alpha;
    }
    
    public boolean equals(Color color) {
        return r == color.r &&
                g == color.g &&
                b == color.b &&
                a == color.a;
    }
    
    public Color clone() {
        return new Color(r, g, b, a);
    }
    
    public static final Color Black = new Color(0, 0, 0);
    public static final Color White = new Color(255, 255, 255);
    public static final Color Red = new Color(255, 0, 0);
    public static final Color Green = new Color(0, 255, 0);
    public static final Color Blue = new Color(0, 0, 255);
    public static final Color Yellow = new Color(255, 255, 0);
    public static final Color Magenta = new Color(255, 0, 255);
    public static final Color Cyan = new Color(0, 255, 255);

}
