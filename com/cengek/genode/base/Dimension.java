package com.cengek.genode.base;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public class Dimension {

    public int width, height;

    public Dimension() {
        width = 0;
        height = 0;
    }

    public Dimension(Dimension dimension) {
        this.width = dimension.width;
        this.height = dimension.height;
    }

    public Dimension(int x, int y) {
        this.width = x;
        this.height = y;
    }

    public void add(Dimension dimension) {
        this.width += dimension.width;
        this.height += dimension.height;
    }
    
    public void subtract(Dimension dimension) {
        this.width -= dimension.width;
        this.height -= dimension.height;
    }
    
    public void multiply(Dimension dimension) {
        this.width *= dimension.width;
        this.height *= dimension.height;
    }
    
    public void divide(Dimension dimension) {
        this.width /= dimension.width;
        this.height /= dimension.height;
    }
    
    public void add(int n) {
        this.width += n;
        this.height += n;
    }
    
    public void subtract(int n) {
        this.width -= n;
        this.height -= n;
    }
    
    public void multiply(int n) {
        this.width *= n;
        this.height *= n;
    }
    
    public void divide(int n) {
        this.width /= n;
        this.height /= n;
    }
    
    public String toString() {
        return "{" + width + "," + height + "}";
    }
    
    public Dimension clone() {
        return new Dimension(width, height);
    }
    
}
