package com.cengek.genode.tween;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public abstract class Tween implements ITween {
    
    protected boolean _isCompleted;
    
    protected Tween() {
        reset();
    }
    
    public boolean isCompleted() {
        return _isCompleted;
    }
    
    public void reset() {
        _isCompleted = false;
    }
    
    public abstract void update(long delta);
    
}
