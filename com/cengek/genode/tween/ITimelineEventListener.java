/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cengek.genode.tween;

import com.cengek.genode.base.IEventListener;

/**
 *
 * @author Graha
 */
public interface ITimelineEventListener extends IEventListener {
    
    public void onTimelineCompleted();
    
}
