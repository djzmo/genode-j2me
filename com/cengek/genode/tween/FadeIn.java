package com.cengek.genode.tween;


import com.cengek.genode.base.Entity;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public class FadeIn extends Tween {
    
    private long _duration, _elapsed;
    private Entity _target;
    
    public FadeIn(long duration, Entity target) {
        _elapsed = 0;
        _duration = duration;
        _target = target;
    }
    
    public void update(long delta) {
        if(_isCompleted)
            return;
        
        _elapsed += delta;
        
        /*if(_elapsed < _duration) {
            _callback(_elapsed / _duration * 255.0);
        }
        else if(_elapsed >= _duration) {
            _isCompleted = true;
        }*/
    }
    
}
