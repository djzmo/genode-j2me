package com.cengek.genode.tween;


import com.cengek.genode.base.IEventDispatcher;
import com.cengek.genode.base.Pair;
import java.util.Vector;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public class Timeline extends IEventDispatcher {
    
    private Vector _tweens;
    private boolean _isRunning, _isCompleted;
    private long _elapsed;
    
    public Timeline() {
        _tweens = new Vector();
        _isRunning = false;
        _isCompleted = false;
    }
    
    public void add(Tween tween, int delay) {
        _tweens.addElement(new Pair(tween, new Integer(delay)));
    }
    
    public void clear() {
        _tweens.removeAllElements();
        
        _isCompleted = false;
        _isRunning = false;
    }
    
    public void reset() {
        for(int i = 0; i < _tweens.size(); i++) {
            ((Tween)((Pair)_tweens.elementAt(i)).first).reset();
        }
        
        _isCompleted = false;
        _isRunning = false;
    }
    
    public void run() {
        _isRunning = true;
        _isCompleted = false;
        _elapsed = 0;
    }
    
    public void update(long delta) {
        if(!_isRunning || _isCompleted)
            return;
        
        _elapsed += delta;
        int completedTweens = 0;
        
        for(int i = 0; i < _tweens.size(); i++) {
            Pair p = (Pair)_tweens.elementAt(i);
            Tween t = (Tween)p.first;
            int d = ((Integer)p.second).intValue();
            if(!t.isCompleted() && _elapsed >= d)
                t.update(delta);
            else if(t.isCompleted())
                completedTweens++;
        }
        
        if(completedTweens == _tweens.size()) {
            _isRunning = false;
            _isCompleted = true;
            
            if(_eventListener != null)
                ((ITimelineEventListener)_eventListener).onTimelineCompleted();
        }
    }
    
}
