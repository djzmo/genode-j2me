/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cengek.genode.tween;

/**
 *
 * @author Graha
 */
public interface ITween {
    
    public boolean isCompleted();
    public void update(long delta);
    public void reset();

}
