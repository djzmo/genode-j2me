/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cengek.genode.tween;

import com.cengek.genode.base.Entity;
import com.cengek.genode.base.Point;

/**
 *
 * @author Graha
 */
public class Easing extends Tween {

    private long _duration, _elapsed;
    Point _source, _destination;
    Entity _target;
    
    public Easing(Point source, Point destination, long duration, Entity target) {
        _source = source;
        _destination = destination;
        _duration = duration;
        _target = target;
    }
    
    public void update(long delta) {
        if(_isCompleted)
            return;
        
        _elapsed += delta;
        
        long t = _elapsed;
        long d = _duration;
        
        Point a = new Point((int)(-(_destination.x - _source.x) * (Math.cos(3.14 * t / d) - 1.0) + _source.x),
                (int)(-(_destination.y - _source.y) * (Math.cos(3.14 * t / d) - 1.0) + _source.y));
        
        _target.setPosition(a);
        
        if(_elapsed >= _duration)
            _isCompleted = true;
    }
    
}
