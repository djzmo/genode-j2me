/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cengek.genode.ui;

import com.cengek.genode.base.Color;
import com.cengek.genode.base.Dimension;
import com.cengek.genode.base.Point;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Graha
 */
public class Button extends Element {
    
    String _caption;
    Color _blurColor, _focusColor;
    boolean _click;
    
    public Button() {
        super();
        _caption = "";
        _blurColor = Color.White;
        _focusColor = Color.Red;
        _click = false;
    }
    
    public Button(Point position, String caption) {
        super(position, new Dimension(0, 0));
        _caption = caption;
        _blurColor = Color.White;
        _focusColor = Color.Red;
        _click = false;
    }
    
    public void setForegroundColor(Color blurColor, Color focusColor) {
        _blurColor = blurColor;
        _focusColor = focusColor;
    }
    
    public void setCaption(String caption) {
        _caption = caption;
    }
    
    public String getCaption() {
        return _caption;
    }
    
    public boolean onClick() {
        boolean c = _click;
        _click = false;
        return c;
    }
    
    public void notifyClick() {
        _click = true;
    }
    
    public void render(Graphics graphics) {
        if(_visible) {
            if(_focus) {
                graphics.setColor(_focusColor.r, _focusColor.g, _focusColor.b);
            }
            else {
                graphics.setColor(_blurColor.r, _blurColor.g, _blurColor.b);
            }
            
            graphics.drawString(_caption, _position.x, _position.y, 0);
        }
    }
    
    public void update(long delta) {
        
    }
    
}
