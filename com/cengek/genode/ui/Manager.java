package com.cengek.genode.ui;


import com.cengek.genode.base.G;
import com.cengek.genode.base.Keypad;
import com.cengek.genode.base.World;
import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.GameCanvas;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public class Manager {
    
    private World _parent;
    private Vector _elements;
    private int _focus;
    
    public Manager(World parent) {
        _parent = parent;
        _elements = new Vector();
        _focus = -1;
    }
    
    public Element add(Element element) {
        _elements.addElement(element);
        
        if(_focus == -1) {
            _focus = 0;
            ((Element)_elements.elementAt(_focus)).focus();
        }
        
        return element;
    }
    
    public void remove(Element element) {
        _elements.removeElement(element);
    }
    
    public void render(Graphics graphics) {
        for(int i = 0; i < _elements.size(); i++) {
            ((Element)_elements.elementAt(i)).render(graphics);
        }
    }
    
    public void update(long delta) {
        for(int i = 0; i < _elements.size(); i++) {
            ((Element)_elements.elementAt(i)).update(delta);
        }
        
        if(Keypad.isKeyPressed(Keypad.Down) || Keypad.isKeyPressed(Keypad.Right)) {
            if(_focus < _elements.size() - 1) {
                ((Element)_elements.elementAt(_focus)).blur();
                _focus++;
                ((Element)_elements.elementAt(_focus)).focus();
            }
        } else if(Keypad.isKeyPressed(Keypad.Up) || Keypad.isKeyPressed(Keypad.Left)) {
            if(_focus > 0) {
                ((Element)_elements.elementAt(_focus)).blur();
                _focus--;
                ((Element)_elements.elementAt(_focus)).focus();
            }
        }
        else if(Keypad.isKeyPressed(Keypad.Fire) && _elements.elementAt(_focus) instanceof Button) {
            ((Button)_elements.elementAt(_focus)).notifyClick();
        }
    }
    
}
