package com.cengek.genode.ui;


import com.cengek.genode.base.Dimension;
import com.cengek.genode.base.Entity;
import com.cengek.genode.base.IDrawable;
import com.cengek.genode.base.Point;
import javax.microedition.lcdui.Graphics;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public abstract class Element extends Entity {
    
    protected boolean _focus, _visible, _enabled;
    
    public Element() {
        super();
        _focus = false;
    }
    
    public Element(Point position, Dimension dimension) {
        super(position, dimension);
        _focus = false;
    }
    
    public boolean getFocus() {
        return _focus;
    }
    
    public void setFocus(boolean focus) {
        _focus = focus;
    }
    
    public boolean getVisibility() {
        return _visible;
    }
    
    public void setVisibility(boolean visible) {
        _visible = visible;
    }
    
    public void show() {
        _visible = true;
    }
    
    public void hide() {
        _visible = false;
    }
    
    public void focus() {
        _focus = true;
    }
    
    public void blur() {
        _focus = false;
    }
    
    public void enable() {
        _enabled = true;
    }
    
    public void disable() {
        _enabled = false;
    }
    
    public abstract void render(Graphics graphics);
    public abstract void update(long delta);
    
}
