/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cengek.genode.base.Engine;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

/**
 * @author Graha
 */
public class SampleMidlet extends MIDlet {
    
    private Engine _engine;

    public void startApp() throws MIDletStateChangeException {
        if(_engine == null) {
            _engine = new SampleCanvas(this);
            _engine.start();
        }
        
        Display.getDisplay(this).setCurrent(_engine);
    }
    
    public void pauseApp() {
        notifyPaused();
    }
    
    public void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        notifyDestroyed();
    }
}
