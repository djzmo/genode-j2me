
import com.cengek.genode.base.Engine;
import com.cengek.genode.base.G;
import javax.microedition.midlet.MIDlet;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Graha
 */
public class SampleCanvas extends Engine {
    
    public SampleCanvas(MIDlet parent) {
        super(parent);
    }
    
    public void init() {
        super.init();
        G.world = new SampleWorld();
    }
    
}
